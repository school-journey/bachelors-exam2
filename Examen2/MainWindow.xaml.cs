﻿using Examen2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Examen2
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static LinqqDataContext cnx = Connexion.cnx;
        private static bool Ok = false;
        private static bool existeExp = false;
        private static bool existeDest = false;
        private static Personne dest, expe;


        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            Examen2.DTSetMoney dTSetMoney = ((Examen2.DTSetMoney)(this.FindResource("dTSetMoney")));
            // Chargez les données dans la table Personne. Vous pouvez modifier ce code si nécessaire.
            Examen2.DTSetMoneyTableAdapters.PersonneTableAdapter dTSetMoneyPersonneTableAdapter = new Examen2.DTSetMoneyTableAdapters.PersonneTableAdapter();
          
            //dTSetMoneyPersonneTableAdapter.Fill(dTSetMoney.Personne);
            System.Windows.Data.CollectionViewSource personneViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("personneViewSource")));
            personneViewSource.View.MoveCurrentToFirst();
        }

        private void tbxNumExp_LostFocus(object sender, RoutedEventArgs e)
        {
            if(tbxNumExp.Text.Trim().Length > 0)
            {
                try
                {
                    Personne pers = (from p in cnx.Personne where p.numPiece.Equals(tbxNumExp.Text.Trim()) select p).FirstOrDefault();
                    if(pers != null)
                    {
                        expe = pers;
                        tbxNomExp.Text = pers.nom;
                        tbxTelExp.Text = pers.tel;
                        existeExp = true;
                    }
                    else
                    {
                        expe = null;
                        //tbxNomExp.Text = "";
                        //tbxTelExp.Text = "";
                        existeExp = false;

                    }
                }
                catch (Exception)
                {

                }
            }
        }

        private void GenererCode()
        {
            
            if (tbxTelDest.Text.Trim().Length > 3 && tbxTelExp.Text.Trim().Length > 3)
            {
                string code = tbxTelExp.Text.Substring(1, 2) + "" + new Random().Next(0, 100) + "" + tbxTelDest.Text.Substring(1, 2);
                tbxCode.Text = code;
            }
        }

        private bool ControlTotal()
        {
           

            if(Ok && tbxCode.Text.Trim().Length > 0)
            {
                try
                {
                    int.Parse(tbxMontant.Text.Trim());
                    int.Parse(tbxFrais.Text.Trim());
                    btnSend.IsEnabled = true;
                }
                catch (Exception)
                {
                    return false;
                    //btnSend.IsEnabled = false;
                }
                return true;
            }
            else
                {
                    //btnSend.IsEnabled = false;
                    return false;
                }
        }

        private void genererFrais()
        {
            if(tbxMontant.Text.Trim().Length > 0)
            {
                try
                {
                    int montant = int.Parse(tbxMontant.Text.Trim());
                    double frais = Math.Ceiling((double)montant / 1000) * 50;

                    

                    tbxFrais.Text = frais.ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Le montant doit etre une valeur numerique entiere", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                    tbxMontant.Text = "";
                    tbxFrais.Text = "";
                }
            }
        }

        private void Control ()
        {
            if (tbxTelExp.Text.Trim().Length > 0 && tbxNomExp.Text.Trim().Length > 0 && tbxNumExp.Text.Trim().Length > 0
                && tbxTelDest.Text.Trim().Length > 0 && tbxNomDest.Text.Trim().Length > 0)
            {
                Ok = true;
            }
            else
                {
                      Ok = false;
                }
        }

        private void tbxTelDest_LostFocus(object sender, RoutedEventArgs e)
        {
            if (tbxTelDest.Text.Trim().Length > 0)
            {
                try
                {
                    Personne pers = (from p in cnx.Personne where p.tel.Equals(tbxTelDest.Text.Trim()) select p).FirstOrDefault();
                    if (pers != null)
                    {
                        dest = pers;
                        tbxNomDest.Text = pers.nom;
                        existeDest = true;

                    }
                    else
                    {
                        dest = null;
                        tbxNomDest.Text = "";
                        existeDest = false;
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        private void tbxMontant_KeyUp(object sender, KeyEventArgs e)
        {
            genererFrais();
        }

        private void tbxNumExp_TextChanged(object sender, TextChangedEventArgs e)
        {
            GenererCode();
        }

        private void tbxTelDest_TextChanged(object sender, TextChangedEventArgs e)
        {
            GenererCode();
        }


        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            if(ControlTotal())
            {
              
//                    cnx.Connection.BeginTransaction();
                    if (expe == null)
                    {
                       

                        expe = new Personne { nom = tbxNomExp.Text.Trim(), tel = tbxTelExp.Text.Trim(), numPiece = tbxNumExp.Text.Trim() };
                        cnx.Personne.InsertOnSubmit(expe);
                        cnx.SubmitChanges();
                        expe = (Personne) (from p in cnx.Personne where p.numPiece.Equals(tbxNumExp.Text.Trim()) select p).FirstOrDefault();
                        
                    }

                    if (dest == null)
                    {
                        dest = new Personne { nom = tbxNomDest.Text.Trim(), tel = tbxTelDest.Text.Trim(), numPiece = tbxTelDest.Text.Trim() };
                        cnx.Personne.InsertOnSubmit(dest);
                        cnx.SubmitChanges();
                        dest = (Personne)(from p in cnx.Personne where p.numPiece.Equals(tbxTelDest.Text.Trim()) select p).FirstOrDefault();

                        
                    }

                    Transaction tran = new Transaction { code = tbxCode.Text, date = DateTime.Now, etat = "Non retire", 
                        frais = int.Parse(tbxFrais.Text), idDestinataire = dest.idPersonne, idExpediteur = expe.idPersonne
                    , montant = int.Parse(tbxMontant.Text.Trim()), Personne = expe, Personne1 = dest };

                    cnx.Transaction.InsertOnSubmit(tran);
                    cnx.SubmitChanges();

                    

                    MessageBox.Show("Insertion effectue", "Ok", MessageBoxButton.OK, MessageBoxImage.Information);
               

            }
            else
            {
                MessageBox.Show("Remplissez tous les champs.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void tbxMontant_TextChanged(object sender, TextChangedEventArgs e)
        {
            genererFrais();
        }

        private void tbxNomExp_TextChanged(object sender, TextChangedEventArgs e)
        {
            Control();
        }

        private void tbxTelExp_TextInput(object sender, TextCompositionEventArgs e)
        {
            Control();
        }

        private void tbxNomDest_TextChanged(object sender, TextChangedEventArgs e)
        {
            Control();
        }


        
        


    }
}
