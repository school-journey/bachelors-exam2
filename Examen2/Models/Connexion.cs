﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen2.Models
{
    class Connexion
    {
        private static LinqqDataContext _cnx;

        public static LinqqDataContext cnx 
        {

            get
            {
                if(_cnx == null)
                    _cnx = new LinqqDataContext();

                return _cnx;
            }

        }

    }
}
